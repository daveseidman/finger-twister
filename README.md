# Finger Twister  

Multiplayer, single-screen game to be played on large phone or iPad.  
Following the same rules as the classic game "twister" users take
turns placing their fingers on colored dots.
